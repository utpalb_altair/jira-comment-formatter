# New in v0.3.7
**Extension working fine as of November 28, 2023**

The extension keeps breaking after every few weeks because of class name changes made by JIRA to the ticket pages. The code now has been modified to have no dependency on generated class names. Hopefully, this time the extension should keep working for longer without any issues.

# Install the extension
1. If you got it as *.zip*, extract it
2. Go to extensions page of your browser, *edge://extensions* or *chrome://extensions* (for Edge and Chrome respectively)
3. Enable *Developer Mode*
4. Click on "Load unpacked" and choose the extracted folder in *step 1*

# Pin the extension

## Edge
![Pin in Edge](help/images/pin-edge.png "Pin in Edge")

## Chrome
![Pin in Chrome](help/images/pin-chrome.png "Pin on Chrome")

# Format comments
Just click on the extension icon to format all comments in a JIRA ticket page

# Optional
By default, comments made after JIRA cloud migration will not be formatted (because of certain issues observed). If you still need to **force all comments to be formatted**,
1. Right click on the extension icon
2. Choose "Extension options"
3. Uncheck "*Don't format comments after migration date*"

![Extension options](help/images/options.png "Extension options")
