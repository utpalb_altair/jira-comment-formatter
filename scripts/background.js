const allowedSites = [
    "https://altairengineering.atlassian.net/browse",
    "https://altairengineering.atlassian.net/issues"
];

function isAllowed(url) {
    for (const site of allowedSites) {
        if (url.startsWith(site)) {
            return true;
        }
    }

    return false;
}

chrome.action.onClicked.addListener(async tab => {
    if (isAllowed(tab.url)) {
        try {
            await chrome.tabs.sendMessage(tab.id, {'format': true})
        } catch(err) {
            console.error(err);
        }
    }
});