/*
# Manual format will format new or old comments depending on the settings.
- This is because a comment could be a mixture of old and newly edited parts
- A new comment (in the page) could also come up as part of "View older comment" button

# Auto format will only kickoff during first load
- It will modify only if the comment is older than the JIRA cloud migration date
*/

/**
 * Converts date in this form 'July 27, 2023 at 12:30 PM' to timestamp
 * @param {*} dateString precisely in this form 'July 27, 2023 at 12:30 PM GMT'.
 * Timezone GMT or UTC are optional
 * @returns date's timestamp
 */
function getDate(dateString) {
    // dateString = 'April 24, 2023 at 10:41 PM';
    let [mm, dd, yy, _, tt, ap ] = dateString.split(' ');

    dd = parseInt(dd);

    return Date.parse(`${dd} ${mm} ${yy} ${tt} ${ap}`);
}

// constants
const COMMENTS_LIST_SEL = "issue.activity.comments-list";
const COMMENT_DIVS_SEL = ".ak-renderer-document";
const MIGRATION_DATE = getDate("July 27, 2023 at 12:30 PM");
const WAIT_TIME = 2000; // in ms
const MAX_TRIES = 3;

// We want to keep this set even if you click the button multiple times
const processedDivs = new Set();

let timeOutCounter = MAX_TRIES;
const txt = document.createElement("textarea");

function getParentDivsOfComments(element) {
    try {
        // This will give the single parent div of all the comment divs
        let parent = element.querySelector(`[data-testid="${COMMENTS_LIST_SEL}"]`);
        return parent.childNodes;
    } catch {
        return []
    }
}

function getAllCommentDivs(element) {
    return element.querySelectorAll(COMMENT_DIVS_SEL);
}

const formatHtml = html => {
    txt.innerHTML = html;
    return txt.innerText;
}

/**
 * Format all unprocessed comments under the supplied divs
 * @param {HTMLDivElement} divs  
 */
function formatComments(divs) {
    for (const div of divs) {
        if (processedDivs.has(div)) {
            continue;
        }

        processedDivs.add(div);

        for (const child of div.children) {
            child.innerHTML = formatHtml(child.innerText);
        }
    }
}

/**
 * Returns the date span inside parent element
 * Performs pattern matching to ascertain whether it is the 
 * span containing a date
 * @param {HTMLElement} element 
 */
function getDateSpan(element) {
    const allSpans = element.querySelectorAll('span');
    console.log(allSpans);
    for(const span of allSpans) {
        try {
            console.log(span.innerText);
            const isDateSpan = /^\w+\s+\d+\s*,\s+\d+\s+at\s+\d+:\d+\s+\w+$/.test(span.innerText);
            if (isDateSpan)
                return span;
        } catch (error) {
            continue;
        }
    }

    return null;
}

/**
 * This will format unprocessed comments
 */
function formatAllComments(dontFormatAfterMigration) {
    if (!dontFormatAfterMigration) {
        const divs = getAllCommentDivs(document);
        formatComments(divs);
        return;
    }

    const commentDivs = getParentDivsOfComments(document);

    for (const commentDiv of commentDivs) {
        const dateSpan = getDateSpan(commentDiv);

        // dateSpan can be null for very recent comments where the date
        // is written like, "5 minutes ago". In which case, it is 
        // obviously a comment after migration date and thus, not
        // to be processed.
        if (!dateSpan)
            continue;

        const date = getDate(dateSpan.innerText);

        // Don't do anything if user has selected to not auto format
        // after migration date
        if (isNaN(date) || date > MIGRATION_DATE) {
            console.log("Didn't process");
            continue;
        }

        const divs = getAllCommentDivs(commentDiv);

        formatComments(divs);
    }
}

chrome.runtime.onMessage.addListener(async msg => {
    let format = msg['format'] ? true: false;
    if (format) {
        let options = getDefaultOptions();
        try {
            let res = await chrome.storage.local.get(Object.keys(options));
            if (res && Object.keys(res).length !== 0) {
                options = res;
            }
        } catch(err) {
            console.log(err);
        }
        formatAllComments(options['dont-manual-format-after-migration']);
    }
    return Promise.resolve({res: 'formatted'});
});

function getDefaultOptions() {
    const defaultOptions = {
        'auto-format': true,
        'dont-auto-format-after-migration': true,
        'dont-manual-format-after-migration': true 
    }

    return {...defaultOptions};
}

async function formatCommentsOnPageLoad() {
    let options = getDefaultOptions();
    try {
        let res = await chrome.storage.local.get(Object.keys(options));
        if (res && Object.keys(res).length !== 0) {
            options = res;
        }

        // If auto-format is off, don't format and just return
        if (!options['auto-format'])
            return;

    } catch(err) {
        console.log(err);
    }
    formatAllComments(options['dont-auto-format-after-migration']);
}

async function waitForCommentsToLoad() {
    if (timeOutCounter-- <= 0)
        return;

    if (getParentDivsOfComments(document).length === 0) {
        setTimeout(waitForCommentsToLoad, WAIT_TIME);
    } else {
        await formatCommentsOnPageLoad();
    }
}

window.addEventListener('load', async () => {
    // If the comment divs have not yet been loaded, wait for sometime
    if (getParentDivsOfComments(document).length === 0) {
        setTimeout(waitForCommentsToLoad, WAIT_TIME);
    }
    else {
        await formatCommentsOnPageLoad();
    }
});

// document.addEventListener('readystatechange', () => {
//     console.log(`Changing ${document.readyState}`);
// })