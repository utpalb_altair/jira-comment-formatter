const autoFormat = document.getElementById('enable-auto-format');
const dontAfterMigrationAuto = document.getElementById('enable-afterdate-comments-auto');
const dontAfterMigrationManual = document.getElementById('enable-afterdate-comments-manual');
const resetButton = document.getElementById('reset-button');

// Returns a clone
function getDefaultOptions() {
    const defaultOptions = {
        'auto-format': true,
        'dont-auto-format-after-migration': true,
        'dont-manual-format-after-migration': true 
    }

    return {...defaultOptions};
}

function setWidgetStates(options) {
    autoFormat.checked = options['auto-format'];
    dontAfterMigrationAuto.checked = options['dont-auto-format-after-migration'];
    dontAfterMigrationManual.checked = options['dont-manual-format-after-migration'];

    onAutoFormatChange();
}

function getWidgetStates() {
    return {
        'auto-format': autoFormat.checked,
        'dont-auto-format-after-migration': dontAfterMigrationAuto.checked,
        'dont-manual-format-after-migration': dontAfterMigrationManual.checked
    }
}

// Write settings to chrome storage
async function saveAllOptions() {
    const options = getWidgetStates();
    try {
        await chrome.storage.local.set(options);
    } catch (err){
        console.log(err);
    }
}

// Called when page loads
async function restoreOptions() {
    let options = getDefaultOptions();
    try {
        const res = await chrome.storage.local.get(Object.keys(options));
        if (res && Object.keys(res).length !== 0) {
            options = res;
        }
    } catch(err) {
        console.log(err);
    }
    // console.log(getDefaultOptions());
    setWidgetStates(options);
}

// Call it whenever you're changing autoFormat state
function onAutoFormatChange() {
    if (autoFormat.checked) {
        dontAfterMigrationAuto.disabled = false;
    } 
    else {
        dontAfterMigrationAuto.disabled = true;
    }
}

autoFormat.addEventListener('change', e => {
    onAutoFormatChange();

    saveAllOptions();
});

dontAfterMigrationAuto.addEventListener('change', e => {
    saveAllOptions();
})

dontAfterMigrationManual.addEventListener('change', e => {
    saveAllOptions();
})

resetButton.addEventListener('click', async e => {
    setWidgetStates(getDefaultOptions());
    try {
        await chrome.storage.local.clear();
    } catch(err) {
        console.log(err);
    }
})

document.addEventListener('DOMContentLoaded', () => {
    restoreOptions();
})