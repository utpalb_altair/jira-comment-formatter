# Change Log

## v0.3.0

### Added
- Added ability to auto format comments on page load
- Added an options page

### Changed
- By default, comments made after JIRA cloud migration will not be formatted

## v0.2.0

### Added
- Added icons for the extension

### Changed

- Now format comments by clicking on the extension icon. No popup window anymore.
- Changed extension description

### Fixed

- Issue where clicking on format again used to remove HTML *bold* texts and other HTML tags
